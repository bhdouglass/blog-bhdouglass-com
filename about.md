---
layout: page
title: About
permalink: /about/
---

This blog is for quick updates for various projects
and apps that I have been working on. And occasionally
some random other things. For a full list of my projects,
check out my [website](http://bhdouglass.com/) or my
[GitLab Profile](https://gitlab.com/bhdouglass).
