FROM jekyll/jekyll
LABEL maintainer="Brian Douglass"

RUN apk --no-cache add nodejs npm
RUN npm install -g surge
