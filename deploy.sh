#!/bin/bash

rm -rf _site

docker run --rm \
  --volume="$PWD:/srv/jekyll" \
  -it jekyll/jekyll \
  jekyll build

cd _site
surge . --domain https://blog.bhdouglass.com
