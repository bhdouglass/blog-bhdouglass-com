---
layout: post
title:  "Various Updates (Oct 2016)"
date:   2016-10-11 11:17:00 -0400
categories: various
---

It's been a while since I've updated my blog. I've been working hard at my day
job and freelance gigs that I haven't had much time for anything else. I was
able to take the opportunity to make some small updates various projects last night.

Both my watchfaces, [Simply Light](http://bhdouglass.com/pebble/simply-light.html)
and [Hazy](http://bhdouglass.com/pebble/hazy.html) have been updated for Pebble
sdk v4.1. This includes support for the new Timeline Quick View feature that Pebble
introduced.

In addition to the sdk upgrade, Hazy is now Free and
[Open Source](https://github.com/bhdouglass/hazy)! It was an interesting
"experiment", so a huge thank you to the few people that did buy the watchface!

Thanks to [Uptime Robot](https://uptimerobot.com/), [uApp Explorer](https://uappexplorer.com/)
and the [OpenStore](https://open.uappexplorer.com/) both have a shiny new
[status page](http://status.uappexplorer.com/). You can find links to this page
at the bottom of both websites.

Since Forecast.io updated their branding to [Dark Sky](https://darksky.net/dev/)
I made a few updates to my [weather-man npm module](https://github.com/bhdouglass/weather-man).
This includes the Dark Sky changes as well as a
[pull request](https://github.com/bhdouglass/weather-man/pull/3) with some tweaks
and features from another developer.
