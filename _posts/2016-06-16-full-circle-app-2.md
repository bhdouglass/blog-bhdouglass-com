---
layout: post
title:  "Full Circle 2.0 (Convergence)"
date:   2016-06-16 22:55:00 -0400
categories: fullcircle ubuntu
---

I recently finished version 2.0 of the official
[Full Circle app](https://uappexplorer.com/app/fullcircle.bhdouglass)
for Ubuntu Touch. This version of the app has been updated to the latest Ubuntu
SDK and includes convergence features. Updating to the latest SDK was fairly simple
after following a QML pattern that I picked up working on the
[Rockwork app](http://rockwork.bhdouglass.com/). Convergence was even easier to
implement as Ubuntu's AdaptivePageLayout is very slick. If you're an Ubuntu
Touch app developer I recommend trying it out!

Download the [Full Circle app](https://uappexplorer.com/app/fullcircle.bhdouglass)
and look forward to future improvements!

{: .center}
![Full Circle](/assets/fullcircle/fullcircle2.png)
