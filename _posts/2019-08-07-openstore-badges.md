---
layout: post
title:  "OpenStore Badges"
date:   2019-08-07 23:22:00 -0400
categories: openstore ubuntu-touch
---

A few weeks back (before vacation and a business trip), Ben from the OpenStore
telegram group produced some great badges for the OpenStore. Here are a few examples
in different languages:

{: .center}
[![OpenStore](https://open-store.io/badges/en_US.png)](https://open-store.io/app/openstore.openstore-team)

{: .center}
[![OpenStore](https://open-store.io/badges/de.png)](https://open-store.io/app/openstore.openstore-team)

{: .center}
[![OpenStore](https://open-store.io/badges/es.png)](https://open-store.io/app/openstore.openstore-team)

You may have seen badges around the internet for other app store (App Store, Play Store, etc)
but now you can let your users know they can download your app from the OpenStore.
To get the badge for your specific app, go to manage the app in the OpenStore and
check out the new "Badge" tab. Alternatively you can find generic badges
on the [badges page](https://open-store.io/badge).

A huge thank you to Ben and all the OpenStore contributors and app developers!
I'll be updating my READMEs in all my apps to include the new badge!
